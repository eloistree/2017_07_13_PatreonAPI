﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pledge  {

    public int _id;
    public int _amountCents;
    public int _pledgeCapCents;
    public Timestamp _whenStarted ;
    public Timestamp _whenDeclined;
    public string _receiverId;
    public string _creditorId;
    public string _rewardId;
}
