﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlidersToAutoResizeGrid : MonoBehaviour {

    public Slider _colSlider;
    public Slider _rowSlider;
    public Slider _multiplicatorSlider;
    public AutoResizeGrid _autoResized;

    void Start()
    {
        _colSlider.onValueChanged.AddListener(SetValue);
        _rowSlider.onValueChanged.AddListener(SetValue);
        _multiplicatorSlider.onValueChanged.AddListener(SetValue);
        SetValue(0);
    }
    public int GetMultiplicator() {
        return (int)_multiplicatorSlider.value;
    }
    private void SetValue(float value)
    {

        if (_rowSlider != null && _autoResized != null && _multiplicatorSlider != null)
        {
            _autoResized.SetRow(((int)_colSlider.value) * GetMultiplicator());
            _autoResized.SetColumn(((int)_rowSlider.value) * GetMultiplicator());
        }   
    }
    
}
