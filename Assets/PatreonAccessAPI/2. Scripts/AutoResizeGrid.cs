﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoResizeGrid : MonoBehaviour {
    public RectTransform _rectReference;
    public GridLayoutGroup _gridLayout;

    [Range(1,64)]
    public int _wantedCol = 16;

    [Range(1, 64)]
    public int _wantedRow = 10;
    
	private IEnumerator Start () {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);
            _debug = GetRatioFormat(_rectReference.rect.width, _rectReference.rect.height);
            AutoResized();

        }
    }

    private void OnValidate()
    {
        AutoResized();
    }

    [Header("Debug")]
    public string _debug = "";
    [Space(10)]
    public float minRangeHorz;
    public float maxRangeHorz;
    public float minRangeVert;
    public float maxRangeVert;
    [Space(10)]
    public float width;
    public float height;
    [Space(10)]
    public int minCell;
    public int maxCell;
    [Space(10)]
    public float widthToApply;

  

    private void AutoResized()
    {
        width = _rectReference.rect.width;  //Screen.width;
        height = _rectReference.rect.height; //Screen.height;

        minRangeHorz = width / (_wantedCol + 1);
        maxRangeHorz = width / _wantedCol;
        minRangeVert = height / (_wantedRow + 1);
        maxRangeVert = height / _wantedRow;

        
        minCell = _wantedCol < _wantedRow ? _wantedCol : _wantedRow;
        maxCell = _wantedCol < _wantedRow ? _wantedRow : _wantedCol;

        if (_wantedCol == _wantedRow)
        {
            _gridLayout.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
            widthToApply = maxRangeVert < maxRangeHorz ? maxRangeVert : maxRangeHorz ;

        }
        else if (_wantedCol < _wantedRow)
        {
            _gridLayout.constraint = GridLayoutGroup.Constraint.FixedColumnCount;

            widthToApply = maxRangeVert;
            //   widthToApply = minRangeHorz;
        }
        else
        {
            _gridLayout.constraint = GridLayoutGroup.Constraint.FixedRowCount;

            widthToApply = maxRangeHorz;
            //  widthToApply =  minRangeVert;
        }

        _gridLayout.constraintCount = minCell;


        if (_rectReference == null)
            _rectReference = GetComponent<RectTransform>();
        if (_gridLayout == null)
            _gridLayout = GetComponent<GridLayoutGroup>();

        


        _gridLayout.cellSize = new Vector2(widthToApply, widthToApply);


        //_gridLayout.cellSize = new Vector2(_rectReference.rect.width/_col, _rectReference.rect.height/_row );
        //_gridLayout.spacing = new Vector2(_rectReference.rect.width / _col , _rectReference.rect.height / _row);

    }
    public void SetColumn(int col)
    {
        if (_wantedCol != col) { 
            _wantedCol = (int) col;
            AutoResized();
        }

    }
    public void SetRow(int row)
    {
        if (_wantedRow != row)
        {
            _wantedRow = (int) row;
            AutoResized();
        }

    }

    public bool NextPage() {

        UI_Avatars[] elements= _gridLayout.GetComponentsInChildren<UI_Avatars>();
        int maxInder = _wantedCol * _wantedRow;
        for (int i = 0; i < elements.Length && i < maxInder; i++)
        {
            elements[i].SetParent(_gridLayout.transform, true);
        }
        elements = _gridLayout.GetComponentsInChildren<UI_Avatars>();

        return elements.Length > 0;
    }

   


    private string GetRatioFormat(float width, float height)
    {
        float gcdPrecision = 10f;
        float ratio = (width / height); // ((int)((width / height)*100f))/100f;
        float rounderRatio = (Mathf.Round((width / height)* gcdPrecision))/ gcdPrecision;


        int gcd = GCD((int) width, (int) height);

        float rw = rounderRatio * gcdPrecision;
        float rh = gcdPrecision;
        int gcdDecimal = GCD( (int) rw, (int)gcdPrecision);
        rw /= gcdDecimal;
        rh /= gcdDecimal;

       //_wantedCol = (int)rw; _wantedRow = (int)rh;
        return string.Format("{0}:{1} (R:{2} GCD:{3})", rw, rh, ratio,gcd);


    }
    private static int GCD(int  a, int b)
    {
        while (a != 0 && b != 0)
        {
            if (a > b)
                a %= b;
            else
                b %= a;
        }

        return a == 0 ? b : a;
    }
}
