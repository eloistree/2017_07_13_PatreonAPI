﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;



public class ImageDownloader : MonoBehaviour {

 
   
    

    public struct LoadInWaiting {
        public LoadInWaiting(string url, OnTextureLoaded createdTexture, OnError onError)
        {
            _url = url;
            _createdTexture = createdTexture;
            _onError = onError;
        }
        public string _url;
        public OnTextureLoaded _createdTexture;
        public OnError _onError;
    }


    public delegate void OnTextureLoaded(Texture texture);
    public delegate void OnSpriteLoaded(Sprite sprite);
    public delegate void OnImageLoaded(Image image);

    public delegate void OnError(string message);

    public static void Load(string url, RawImage toApplyOn, OnError onError = null)
    {
        OnTextureLoaded onLoaded = delegate (Texture text)
        {
            toApplyOn.texture = text;
        };
        Load(url, onLoaded, onError);
    }

    public static void Load(string url, GameObject toApplyOn, OnError onError = null)
    {
        Load(url, toApplyOn.GetComponent<Renderer>(), onError);
    }
    public static void Load(string url, Renderer toApplyOn, OnError onError = null)
    {
        Load(url, toApplyOn.material, onError);
    }
    public static void Load(string url, Material toApplyOn , OnError onError = null)
    {
        Material toApplyOnMat = toApplyOn;
        OnTextureLoaded onLoaded = delegate (Texture text)
        {
            toApplyOnMat.mainTexture = text;
        };
        Load(url, onLoaded, onError);
    }
   


    public static void Load(string url, OnTextureLoaded createdTexture, OnError onError=null ) {
         CoroutineManager.StartCoroutineAsap(LoadTextureIterator(url, createdTexture, onError));

    }
    

     private static  IEnumerator LoadTextureIterator(string url, OnTextureLoaded createdTexture, OnError onError = null) {
        
        

            WWW www = new WWW(url);
            yield return www;
        try
        {
          
            if (www == null)
            {
                onError("WWW failed !");
                yield break;
            }
            else if (!string.IsNullOrEmpty(www.error))
            {
                onError(www.error);
                yield break;
            }

            //Not my code: http://answers.unity3d.com/questions/844423/wwwtext-not-reading-utf-8-text.html
            string text = Encoding.UTF8.GetString(www.bytes, 3, www.bytes.Length - 3);
          

            if (text.IndexOf("<html") >0)
            {
                onError("That a HTML file and not a image: " + url);
            }
            else if (text.Equals("forbidden"))
            {
                onError("Texture access was forbidden: " + url);
            }
            else if (www.texture == null)
            {

                onError("Not texture loaded for: " + url);
            }
            else
            {
                createdTexture(www.texture);
            }

        }
        catch (Exception )
        {

            Debug.Log("2. " + url);
            Debug.Log("3. " + www.text);
            onError("Texture access was forbidden: " + url);
            Debug.Break();
        }
        finally {
        }
    
    }

}

public class ImageGenerator {

    public enum ImageSize : int { x2 = 2, x4 = 4, x8 = 8, x16 = 16, x32 = 32, x64 = 64, x128 = 128, x256 = 256, x512 = 512, x1024 = 1024, x2048 = 2048, x4096 = 4096, x8192 = 8192 }
    public static Texture2D CreateRandomTexture(ImageSize size)
    {
        return CreateRandomTexture((int)size, (int)size);
    }
    public static Texture2D CreateRandomTexture(int width, int height)
    {
        Texture2D texture = new Texture2D(width, height);
        Color[] data = new Color[width * height];
        for (int pixel = 0; pixel < data.Length; pixel++)
        {
            data[pixel] = RandomColor();
        }
        texture.SetPixels(data);
        texture.Apply();

        return texture;
    }
    public static float RandomRange() { return UnityEngine.Random.Range(0f, 1f); }
    public static Color RandomColor(float alpha = 1f)
    {
        return new Color(RandomRange(), RandomRange(), RandomRange(), alpha);
    }
    public static Texture2D CreateTexture(int width, int height, Func<int, Color> paint)
    {
        Texture2D texture = new Texture2D(width, height);
        Color[] data = new Color[width * height];
        for (int pixel = 0; pixel < data.Length; pixel++)
        {
            data[pixel] = paint(pixel);
        }
        texture.SetPixels(data);
        texture.Apply();

        return texture;
    }

   
}



