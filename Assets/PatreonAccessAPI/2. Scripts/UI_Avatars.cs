﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UI_Avatars : MonoBehaviour, IPointerClickHandler
{

    public RectTransform _panelRoot;
    public RawImage _targetToApply;

    public List<string> _imageUrls;
    public int _currentIndex=0;
    public int _index;

    public void Awake()
    {
        SwitchNext();
    }

    public void SetImages(List<string> imageUrls ) {
        if (_imageUrls == null ) {
            _imageUrls = new List<string>();
        }
        _imageUrls = imageUrls;
        SwitchNext();


    }
    public void SwitchNext()
    {
        if (_index >= _imageUrls.Count)
        {
            _index = 0;
        }
        if (_imageUrls.Count < 1)
        {
            _targetToApply.texture = ImageGenerator.CreateRandomTexture(ImageGenerator.ImageSize.x32);
            return;
        }
        string url = _imageUrls[_index];
        if ( ! string.IsNullOrEmpty(url))
        {
            gameObject.name = "Avatar(" + _index + "):" + url;
        }
        ImageDownloader.Load(url, SetAvatarWithTexture, DestroyOnError);
        _index++;
    }

    private void SetAvatarWithTexture(Texture texture)
    {
        _targetToApply.texture = texture;
    }

    private void DestroyOnError(string message)
    {
        if(_currentIndex>=0 && _currentIndex<_imageUrls.Count)
            _imageUrls.RemoveAt(_currentIndex);

        if (_imageUrls.Count < 1)
            Destroy(this.gameObject);
        else SwitchNext();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
            Destroy(this.gameObject);
        if(eventData.button == PointerEventData.InputButton.Left)

            SwitchNext();

    }

    public void SetParent(Transform parent, bool withDeactivate=true)
    {
        _panelRoot.SetParent(null);
        _panelRoot.SetParent(parent);
        _panelRoot.gameObject.SetActive(!withDeactivate);


    }


}